package com.my.touragency.commn.creators;

import com.my.touragency.domain.User;
import com.my.touragency.domain.UserRole;

/**
 * Created by Stein on 12.05.17.
 */
public class UserCreator implements Creator<User> {
    private User user;

    public UserCreator() {
        user = new User();
    }

    public UserCreator withId(Long id) {
        user.setId(id);
        return this;
    }

    public UserCreator withName(String name) {
        user.setUsername(name);
        return this;
    }

    public UserCreator withEmail(String email) {
        user.setEmail(email);
        return this;
    }

    public UserCreator withPassword(String password) {
        user.setPassword(password);
        return this;
    }

    public UserCreator withRole(UserRole role) {
        user.setRole(role);
        return this;
    }

    public UserCreator getUserCreator() {
        return new UserCreator();
    }

    @Override
    public User create() {
        return user;
    }

}
