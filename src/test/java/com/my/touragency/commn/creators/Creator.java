package com.my.touragency.commn.creators;

/**
 * Created by Stein on 12.05.17.
 */
public interface Creator<T> {
    T create();
}
