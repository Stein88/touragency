package com.my.touragency.service.user;

import com.my.touragency.common.Model;
import com.my.touragency.common.constants.Page;
import com.my.touragency.persistance.dao.user.UserDao;
import com.my.touragency.service.parsers.StringParser;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by Stein on 12.05.17.
 */
public class UserServiceTest {
    private static final String NAME = "username";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";

    private UserService userService;
    private UserDao userDao;
    private Model model;

    @Before
    public void setupUserService() {
        model = Mockito.mock(Model.class);
        userDao = Mockito.mock(UserDao.class);
        UserServiceImpl userServiceIml = new UserServiceImpl();
        userServiceIml.setUserDao(userDao);
        userService = userServiceIml;
    }


    @Test
    public void testLoginWhenUserNotExist() throws Exception {
        when(model.findParameter(eq(EMAIL), any(StringParser.class))).thenReturn(EMAIL);
        String actual = userService.loginUser(model);
        assertEquals(Page.LOGIN, actual);
        verify(model).findParameter(eq(EMAIL), any(StringParser.class));
        verify(model).findParameter(eq(PASSWORD), any(StringParser.class));
        verify(model).setAttributes(argThat(new ErrorMatcher()));
        verify(userDao).findByEmail(eq(EMAIL));
        verifyNoMoreInteractions(userDao, model);
    }

    @Test
    public void testLoginSuccessAuthorization() throws Exception {
        when(model.findParameter(eq(EMAIL), any(StringParser.class))).thenReturn(EMAIL);
        when(model.findParameter(eq(PASSWORD), any(StringParser.class))).thenReturn(PASSWORD);


    }

    private class ErrorMatcher extends ArgumentMatcher<Map<String, Object>> {
        @Override
        public boolean matches(Object argument) {
            Map<String, Object> map = (Map<String, Object>) argument;
            return map.size() == 1 && map.containsKey("log_error")
                    && map.containsValue("Incorrect email or password");
        }
    }

}
