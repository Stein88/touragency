<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generic_page>
    <jsp:attribute name="title">Tour list</jsp:attribute>
    <jsp:body>
        <table class="table">
            <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Title</th>
                <th>Route</th>
                <th>Price</th>
                <th>Description</th>
                <th>Discount</th>
                <th>Type</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${tours}" var="tour">
                <tr>
                    <td>
                        <c:if test="${tour.hot}">
                            <span style="color: red;" class="glyphicon glyphicon-fire"></span>
                        </c:if>
                    </td>
                    <td><a href="/app/tour/detail?id=${tour.id}">${tour.id}</a></td>
                    <td>${tour.title}</td>
                    <td>${tour.route}</td>
                    <td>${tour.price}$</td>
                    <td>${tour.description}</td>
                    <td>
                        <c:if test="${tour.discount != 0}">
                            ${tour.discount}%
                        </c:if>
                    </td>
                    <td>${tour.type}</td>
                    <td>
                        <c:if test="${user.role == 'ADMIN'}">
                            <a href="/app/tour/update?id=${tour.id}"
                               class="btn btn-success btn-sm">Update</a>
                        </c:if>
                    </td>
                    <td>
                        <c:if test="${user.role == 'ADMIN'}">
                            <a href="/app/tour/delete?id=${tour.id}"
                               class="btn btn-success btn-sm">Delete</a>
                        </c:if>
                    </td>
                    <c:forEach items="${purchases}" var="purchase">
                        <td>
                            <c:if test="${purchase.status}">
                                <button type="button" class="btn btn-primary disabled">Bought</button>
                            </c:if>
                        </td>
                    </c:forEach>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </jsp:body>
</t:generic_page>
