<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>

<t:generic_page>
    <jsp:attribute name="title">Purchase list</jsp:attribute>
    <jsp:body>
        <table class="table">
            <thead>
            <tr>
                <th>Title</th>
                <th>Quantity</th>
                <th>Total Price</th>
                <th>Last Purchase Date</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${purchases}" var="purchase">
                <tr>
                    <td><a href="/app/purchase/detail?title=${purchase.tour.title}">${purchase.tour.title}</a></td>
                    <td>${purchase.quantity}</td>
                    <td>${purchase.totalPrice}</td>
                    <td><javatime:format value="${purchase.lastPurchaseDate}" style="MS" /></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </jsp:body>
</t:generic_page>
