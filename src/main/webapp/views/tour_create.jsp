<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generic_page>
    <jsp:attribute name="title">Create tour</jsp:attribute>
    <jsp:body>
        <form method="post" action="/app/tour/create">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Create new Tour</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="idInput">ID</label>
                        <input class="form-control" id="idInput" type="text" name="id" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="title">Tour title</label>
                        <input class="form-control" id="title" type="text" name="title"/>
                        <c:if test="${not empty title_error}">
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                    ${title_error}
                            </div>
                        </c:if>
                    </div>
                    <div class="form-group">
                        <label for="route">Tour route</label>
                        <input class="form-control" id="route" type="text" name="route"/>
                        <c:if test="${not empty route_error}">
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                    ${route_error}
                            </div>
                        </c:if>
                    </div>
                    <div class="form-group">
                        <label for="price">Tour price</label>
                        <input class="form-control" id="price" type="text" name="price"/>
                        <c:if test="${not empty price_error}">
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                    ${price_error}
                            </div>
                        </c:if>
                    </div>
                    <div class="form-group">
                        <label for="description">Tour description</label>
                        <textarea class="form-control" rows="5" id="description" name="description"></textarea>
                        <c:if test="${not empty description_error}">
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                    ${description_error}
                            </div>
                        </c:if>
                    </div>
                    <div class="form-group">
                        <label for="discount">Tour discount</label>
                        <input class="form-control" id="discount" type="text" name="discount"/>
                        <c:if test="${not empty discount_error}">
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                    ${discount_error}
                            </div>
                        </c:if>
                    </div>
                    <div class="form-group">
                        <label for="type">Tour type</label>
                        <select class="form-control selectpicker " id="type" name="type">
                            <c:forEach items="${types}" var="type">
                                <option value="${type}">${type.name()}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="active">Active</label>
                        <input type="checkbox" id="active" name="active" value="true">
                        <label for="hot">Hot</label>
                        <input type="checkbox" id="hot" name="hot" value="true">
                    </div>
                    <input type="submit" value="Save" class="btn btn-primary"/>
                </div>
            </div>
        </form>
    </jsp:body>
</t:generic_page>
