<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generic_page>
    <jsp:attribute name="title">Tour Detail</jsp:attribute>
    <jsp:body>
        <form method="post" action="/app/tour/purchase">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Tour Detail</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="id">ID</label>
                        <input class="form-control" id="id" type="text" name="id" value="${tour.id}" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="title">Tour title</label>
                        <input class="form-control" id="title" type="text" name="title" value="${tour.title}" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="route">Tour route</label>
                        <input class="form-control" id="route" type="text" name="route" value="${tour.route}" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="description">Tour description</label>
                        <textarea class="form-control" rows="5" id="description" name="description"
                                  readonly>${tour.description}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="type">Tour type</label>
                        <input class="form-control" id="type" type="text" name="type"
                               value="${tour.type}" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="price">Tour price</label>
                        <input class="form-control" id="price" type="text" name="price" value="${tour.price}" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="discount">Tour discount %</label>
                        <input class="form-control" id="discount" type="text" name="discount"
                               value="${tour.discount}" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="finalPrice">Tour finalPrice</label>
                        <input class="form-control" id="finalPrice" type="text" name="finalPrice" value="${tour.finalPrice}"
                               readonly/>
                    </div>
                    <input type="submit" value="Purchase" class="btn btn-primary"/>
                </div>
            </div>
        </form>
    </jsp:body>
</t:generic_page>

