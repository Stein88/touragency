package com.my.touragency.domain;

/**
 * Created by Stein on 19.04.17.
 */
public enum TourType {
    VOCATION, SHOPPING, SIGHTSEEING;

    public static TourType fromString(String string) {
        TourType result = null;
        for (TourType type : values()) {
            if (type.toString().equalsIgnoreCase(string)) {
                result = type;
                break;
            }
        }
        return result;
    }
}
