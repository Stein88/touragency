package com.my.touragency.domain;

/**
 * Created by Stein on 19.04.17.
 */
public enum UserRole {
    CUSTOMER, TOUR_AGENT, ADMIN;

    public static UserRole fromString(String string) {
        UserRole result = null;
        for (UserRole role : values()) {
            if (role.toString().equalsIgnoreCase(string)) {
                result = role;
                break;
            }
        }
        return result;
    }

}
