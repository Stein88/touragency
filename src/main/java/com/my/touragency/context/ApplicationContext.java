package com.my.touragency.context;

import com.my.touragency.controller.ControllerContext;
import com.my.touragency.persistance.dao.DaoContext;
import com.my.touragency.persistance.dao.DaoFactory;
import com.my.touragency.persistance.dao.DaoFactoryImpl;
import com.my.touragency.service.ServiceContext;
import com.my.touragency.service.ServiceFactory;
import com.my.touragency.service.ServiceFactoryImpl;


/**
 * Created by Stein on 19.04.17.
 */
public class ApplicationContext {
    private static ApplicationContext instance;
    private DaoContext daoContext;
    private ServiceContext serviceContext;
    private ControllerContext controllerContext;

    private ApplicationContext() {
    }

    private static synchronized ApplicationContext getInstance() {
        if (instance == null) {
            instance = new ApplicationContext();
            instance.initDao();
            instance.initService();
            instance.initControllers();
        }
        return instance;
    }

    private void initDao() {
        DaoFactory daoFactory = new DaoFactoryImpl();
        daoContext = DaoContext.init(daoFactory);
    }

    private void initService() {
        ServiceFactory serviceFactory = new ServiceFactoryImpl();
        serviceContext = ServiceContext.init(serviceFactory);
    }

    private void initControllers() {
        controllerContext = ControllerContext.init();
    }

    public static DaoContext getDaoContext() {
        return ApplicationContext.getInstance().daoContext;
    }

    public static ServiceContext getServiceContext() {
        return ApplicationContext.getInstance().serviceContext;
    }

    public static ControllerContext getControllerContext() {
        return ApplicationContext.getInstance().controllerContext;
    }
}


