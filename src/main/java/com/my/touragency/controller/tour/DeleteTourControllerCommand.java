package com.my.touragency.controller.tour;

import com.my.touragency.common.Model;
import com.my.touragency.context.ApplicationContext;
import com.my.touragency.controller.ControllerCommand;
import com.my.touragency.service.ServiceContext;

/**
 * Created by Stein on 07.05.17.
 */
public class DeleteTourControllerCommand implements ControllerCommand {
    private ServiceContext sc = ApplicationContext.getServiceContext();

    @Override
    public String execute(Model model) {
        return sc.getTourService().deleteTour(model);
    }
}
