package com.my.touragency.controller.tour;

import com.my.touragency.common.Model;
import com.my.touragency.context.ApplicationContext;
import com.my.touragency.controller.ControllerCommand;
import com.my.touragency.service.ServiceContext;


/**
 * Created by Stein on 21.04.17.
 */
public class ShowCreateTourPageControllerCommand implements ControllerCommand {
    private ServiceContext sc = ApplicationContext.getServiceContext();

    @Override
    public String execute(Model model) {
        return sc.getTourService().showCreateTourPage(model);
    }
}
