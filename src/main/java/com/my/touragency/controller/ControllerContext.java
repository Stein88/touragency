package com.my.touragency.controller;

import com.my.touragency.common.constants.Page;
import com.my.touragency.controller.purchase.ShowPurchaseListControllerCommand;
import com.my.touragency.controller.purchase.TourPurchaseControllerCommand;
import com.my.touragency.controller.tour.*;
import com.my.touragency.controller.user.CreateUserControllerCommand;
import com.my.touragency.controller.user.LoginUserControllerCommand;
import com.my.touragency.controller.user.ShowAllUsersControllerCommand;
import com.my.touragency.controller.user.ShowLoginPageControllerCommand;
import com.my.touragency.persistance.db.ConnectionManager;
import com.my.touragency.web.DispatcherModel;
import com.my.touragency.web.HttpMethod;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Created by Stein on 20.04.17.
 */
public class ControllerContext {
    private static final Logger LOGGER = Logger.getLogger(ControllerContext.class);
    private Map<String, Map<HttpMethod, ControllerCommand>> controllers;

    public static ControllerContext init() {
        ControllerContext controllerContext = new ControllerContext();
        initControllers(controllerContext);
        return controllerContext;
    }

    private static void initControllers(ControllerContext controllerContext) {
        controllerContext.controllers = new ControllerBuilder()
                .register("/tour/list", new ShowTourListControllerCommand())
                .register("/tour/create", new ShowCreateTourPageControllerCommand())
                .register("/tour/create", HttpMethod.POST, new CreateTourControllerCommand())
                .register("/tour/update", new ShowUpdateTourPageControllerCommand())
                .register("/tour/update", HttpMethod.POST, new UpdateTourControllerCommand())
                .register("/tour/delete", new DeleteTourControllerCommand())
                .register("/tour/detail", new ShowTourDetailControllerCommand())
                .register("/tour/purchase", HttpMethod.POST, new TourPurchaseControllerCommand())
                .register("/purchase/list", new ShowPurchaseListControllerCommand())
                .register("/login", new ShowLoginPageControllerCommand())
                .register("/login", HttpMethod.POST, new LoginUserControllerCommand())
                .register("/registration", HttpMethod.POST, new CreateUserControllerCommand())
                .register("/user/list", new ShowAllUsersControllerCommand())
                .build();

    }

    public void process(HttpMethod httpMethod, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DispatcherModel model = new DispatcherModel();
        model.setParameters(request.getParameterMap());
        model.setSession(request.getSession());
        String path = executeBusinessLogic(httpMethod, model, request);
        for (Map.Entry<String, Object> attributes : model.getAttributes().entrySet()) {
            request.setAttribute(attributes.getKey(), attributes.getValue());
        }

        if (!redirectIfNecessary(path, response)) {
            request.getRequestDispatcher(path).forward(request, response);
        }
    }

    private String executeBusinessLogic(HttpMethod httpMethod, DispatcherModel model, HttpServletRequest request) {
        ControllerCommand controller = findController(request.getPathInfo(), httpMethod);
        if (controller != null) {
            try {
                String path = controller.execute(model);
                ConnectionManager.getInstance().commit();
                return path;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                ConnectionManager.getInstance().rollback();
                return Page.ERROR;
            } finally {
                ConnectionManager.getInstance().close();
            }
        } else {
            return Page.BAD_URL;
        }
    }

    private boolean redirectIfNecessary(String url, HttpServletResponse response) throws ServletException, IOException {
        if (url.startsWith("redirect:")) {
            response.sendRedirect(url.substring(url.indexOf(":") + 1));
            return true;
        }
        return false;
    }

    private ControllerCommand findController(String path, HttpMethod httpMethod) {
        Map<HttpMethod, ControllerCommand> controllersMap = controllers.get(path);
        ControllerCommand controller = null;
        if (controllersMap != null && controllersMap.containsKey(httpMethod)) {
            controller = controllersMap.get(httpMethod);
        }
        return controller;
    }
}
