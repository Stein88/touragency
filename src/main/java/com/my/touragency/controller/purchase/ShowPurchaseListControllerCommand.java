package com.my.touragency.controller.purchase;

import com.my.touragency.common.Model;
import com.my.touragency.context.ApplicationContext;
import com.my.touragency.controller.ControllerCommand;
import com.my.touragency.service.ServiceContext;

/**
 * Created by Stein on 09.05.17.
 */
public class ShowPurchaseListControllerCommand implements ControllerCommand {
    private ServiceContext sc = ApplicationContext.getServiceContext();

    @Override
    public String execute(Model model) {
        return sc.getPurchaseService().showPurchaseListPage(model);
    }
}
