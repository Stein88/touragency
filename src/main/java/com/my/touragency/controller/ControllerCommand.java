package com.my.touragency.controller;

import com.my.touragency.common.Model;

/**
 * Created by Stein on 21.04.17.
 */
public interface ControllerCommand {
    String execute(Model model);
}
