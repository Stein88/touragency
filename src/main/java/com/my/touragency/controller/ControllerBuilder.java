package com.my.touragency.controller;

import com.my.touragency.web.HttpMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stein on 21.04.17.
 */
public class ControllerBuilder {
    private Map<String, Map<HttpMethod, ControllerCommand>> controllers = new HashMap<>();

    public ControllerBuilder register(String path, ControllerCommand controller) {
        return register(path, HttpMethod.GET, controller);
    }

    public ControllerBuilder register(String path, HttpMethod method, ControllerCommand controller) {
        Map<HttpMethod, ControllerCommand> map = findControllerMap(path);
        map.put(method, controller);
        return this;
    }

    public Map<HttpMethod, ControllerCommand> findControllerMap(String path) {
        Map<HttpMethod, ControllerCommand> map;
        if (controllers.containsKey(path)) {
            map = controllers.get(path);
        } else {
            map = new HashMap<>();
        }
        controllers.put(path, map);
        return map;
    }

    public Map<String, Map<HttpMethod, ControllerCommand>> build() {
        return controllers;
    }

}
