package com.my.touragency.controller.user;

import com.my.touragency.common.Model;
import com.my.touragency.context.ApplicationContext;
import com.my.touragency.controller.ControllerCommand;
import com.my.touragency.service.ServiceContext;

/**
 * Created by Stein on 25.04.17.
 */
public class CreateUserControllerCommand implements ControllerCommand {
    private ServiceContext sc = ApplicationContext.getServiceContext();

    @Override
    public String execute(Model model) {
        return sc.getUserService().createUser(model);
    }
}
