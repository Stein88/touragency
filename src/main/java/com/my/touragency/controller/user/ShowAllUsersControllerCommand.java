package com.my.touragency.controller.user;

import com.my.touragency.common.Model;
import com.my.touragency.context.ApplicationContext;
import com.my.touragency.controller.ControllerCommand;
import com.my.touragency.service.ServiceContext;

/**
 * Created by Stein on 04.05.17.
 */
public class ShowAllUsersControllerCommand implements ControllerCommand {
    private ServiceContext sc = ApplicationContext.getServiceContext();

    @Override
    public String execute(Model model) {
        return sc.getUserService().showUserList(model);
    }
}
