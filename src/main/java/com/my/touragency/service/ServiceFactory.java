package com.my.touragency.service;


import com.my.touragency.service.purchase.PurchaseService;
import com.my.touragency.service.tour.TourService;
import com.my.touragency.service.user.UserService;

/**
 * Created by Stein on 20.04.17.
 */
public interface ServiceFactory {
    TourService createTourService();
    UserService createUserService();
    PurchaseService createPurchaseService();

}
