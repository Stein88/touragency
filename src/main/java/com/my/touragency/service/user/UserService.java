package com.my.touragency.service.user;

import com.my.touragency.common.Model;

/**
 * Created by Stein on 25.04.17.
 */
public interface UserService {
    String showLoginPage(Model model);
    String createUser(Model model);
    String loginUser(Model model);
    String showUserList(Model model);

}
