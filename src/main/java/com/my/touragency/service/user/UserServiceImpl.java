package com.my.touragency.service.user;

import com.my.touragency.common.Model;
import com.my.touragency.common.constants.Message;
import com.my.touragency.common.constants.Key;
import com.my.touragency.common.constants.Page;
import com.my.touragency.common.constants.Redirection;
import com.my.touragency.context.ApplicationContext;
import com.my.touragency.persistance.dao.user.UserDao;
import com.my.touragency.domain.User;
import com.my.touragency.domain.UserRole;
import com.my.touragency.service.parsers.EmailParser;
import com.my.touragency.service.parsers.StringParser;
import com.my.touragency.service.parsers.StringRequiredParser;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stein on 25.04.17.
 */
public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);
    private UserDao userDao = ApplicationContext.getDaoContext().getUserDao();
    private static final int MAX_USER_NAME = 10;
    private static final int MAX_USER_PASSWORD = 6;
    private static final String CREATION_USER_LOG_MSG = "Creation user with email %s";
    private static final String USER_ALREADY_CREATED_LOG_MSG = "User with email %s already created, id %d";
    private static final String USER_SUCCESS_CREATED_LOG_MSG = "User with email %s success created, id %d";
    private static final String USER_SUCCESS_LOGIN_LOG_MSG = "User success login, id %d";
    private static final String UNSUCCESS_LOGIN_FOR = "Unsuccess login for user %s";
    private static final String UNKNOWN = "unknown";


    @Override
    public String showLoginPage(Model model) {
        return Page.LOGIN;
    }

    @Override
    public String createUser(Model model) {
        Map<String, Object> errors = new HashMap<>();
        String email = parseEmail(errors, model);
        LOGGER.info(String.format(CREATION_USER_LOG_MSG, email));
        User user = userDao.findByEmail(email);
        if (user != null) {
            LOGGER.warn(String.format(USER_ALREADY_CREATED_LOG_MSG, email, user.getId()));
            errors.put(Key.REGISTRATION_ERROR, Message.USER_ALREADY_CREATED);
        }
        String username = parseUserName(errors, model);
        String password = parsePassword(errors, model);
        if (!errors.isEmpty()) {
            model.setAttributes(errors);
            model.setAttribute(Key.SHOW_REGISTER_FORM, true);
            return Page.LOGIN;
        }
        User newUser = new User(username, email, password, UserRole.CUSTOMER);
        userDao.create(newUser);
        LOGGER.info(String.format(USER_SUCCESS_CREATED_LOG_MSG, email, newUser.getId()));
        return Redirection.LOGIN;
    }

    @Override
    public String loginUser(Model model) {
        Map<String, Object> errors = new HashMap<>();
        String email = model.findParameter(Key.EMAIL, new StringParser());
        String password = model.findParameter(Key.PASSWORD, new StringParser());
        User user = userDao.findByEmail(email);
        if (user != null && user.getPassword().equals(password)) {
            LOGGER.info(String.format(USER_SUCCESS_LOGIN_LOG_MSG, user.getId()));
            model.putSessionAttribute(Key.USER, user);
            return Redirection.TOUR_LIST;
        }
        String userId = user == null ? UNKNOWN : user.getId().toString();
        LOGGER.info(String.format(UNSUCCESS_LOGIN_FOR, userId));
        errors.put(Key.LOGIN_ERROR, Message.INCORRECT_EMAIL_PASSWORD);
        model.setAttributes(errors);
        return Page.LOGIN;
    }

    @Override
    public String showUserList(Model model) {
        model.setAttribute(Key.USERS, userDao.readAll());
        return Page.USER_LIST;
    }

    private String parseUserName(Map<String, Object> errors, Model model) {
        StringRequiredParser userNameParser = new StringRequiredParser(errors);
        userNameParser.setMaxLength(MAX_USER_NAME);
        userNameParser.setMaxLengthErrorMessage(Message.MAX_USER_NAME);
        return model.findParameter(Key.USERNAME, userNameParser);
    }

    private String parsePassword(Map<String, Object> errors, Model model) {
        StringRequiredParser passwordParser = new StringRequiredParser(errors);
        passwordParser.setMaxLength(MAX_USER_PASSWORD);
        passwordParser.setMaxLengthErrorMessage(Message.MAX_USER_PASSWORD);
        return model.findParameter(Key.PASSWORD, passwordParser);
    }

    private String parseEmail(Map<String, Object> errors, Model model) {
        return model.findParameter(Key.EMAIL, new EmailParser(errors));
    }

    void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
