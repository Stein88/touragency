package com.my.touragency.service;

import com.my.touragency.service.purchase.PurchaseService;
import com.my.touragency.service.tour.TourService;
import com.my.touragency.service.user.UserService;

/**
 * Created by Stein on 20.04.17.
 */
public class ServiceContext {
    private TourService tourService;
    private UserService userService;
    private PurchaseService purchaseService;

    public static ServiceContext init(ServiceFactory serviceFactory){
        ServiceContext serviceContext = new ServiceContext();
        serviceContext.tourService = serviceFactory.createTourService();
        serviceContext.userService = serviceFactory.createUserService();
        serviceContext.purchaseService = serviceFactory.createPurchaseService();
        return serviceContext;
    }

    public TourService getTourService() {
        return tourService;
    }

    public UserService getUserService(){
        return userService;
    }

    public PurchaseService getPurchaseService(){
        return purchaseService;
    }

}
