package com.my.touragency.service;

import com.my.touragency.service.purchase.PurchaseService;
import com.my.touragency.service.purchase.PurchaseServiceImpl;
import com.my.touragency.service.tour.TourService;
import com.my.touragency.service.tour.TourServiceImpl;
import com.my.touragency.service.user.UserService;
import com.my.touragency.service.user.UserServiceImpl;

/**
 * Created by Stein on 20.04.17.
 */
public class ServiceFactoryImpl implements ServiceFactory {

    @Override
    public TourService createTourService() {
        return new TourServiceImpl();
    }

    @Override
    public UserService createUserService() {
        return new UserServiceImpl();
    }

    @Override
    public PurchaseService createPurchaseService() {
        return new PurchaseServiceImpl();
    }

}
