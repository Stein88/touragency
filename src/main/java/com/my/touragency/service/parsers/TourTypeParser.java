package com.my.touragency.service.parsers;

import com.my.touragency.domain.TourType;

/**
 * Created by Stein on 24.04.17.
 */
public class TourTypeParser extends Parser<TourType> {

    @Override
    public TourType parse(String key, String[] params) {
        TourType tourTypeEnum = null;
        if (params != null && params.length != 0) {
            tourTypeEnum = Enum.valueOf(TourType.class, params[0]);
        }
        return tourTypeEnum;
    }
}
