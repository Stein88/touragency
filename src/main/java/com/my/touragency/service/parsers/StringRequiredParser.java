package com.my.touragency.service.parsers;

import java.util.Map;


/**
 * Created by Stein on 20.12.16.
 */
public class StringRequiredParser extends Parser<String> {
    private StringParser stringParser = new StringParser();
    private Map<String, Object> errors;
    private String minLengthErrorMessage = "Should be longer";
    private String maxLengthErrorMessage = "Should be shorter";
    private String emptyErrorMessage = "Field is required";
    private int maxLength = Integer.MAX_VALUE;
    private int minLength = 0;

    public StringRequiredParser(Map<String, Object> errors) {
        this.errors = errors;
    }

    @Override
    public String parse(String key, String[] params) {
        String str = stringParser.parse(key, params);
        String errorKey = createErrorKey(key);
        if (errors.containsKey(errorKey)) {
            return str;
        } else if (str == null || str.trim().isEmpty()) {
            errors.put(errorKey, emptyErrorMessage);
        } else if (str.length() < minLength) {
            errors.put(errorKey, minLengthErrorMessage);
        } else if (str.length() > maxLength) {
            errors.put(errorKey, maxLengthErrorMessage);
        }
        return str;
    }

    public void setMinLengthErrorMessage(String minLengthErrorMessage) {
        this.minLengthErrorMessage = minLengthErrorMessage;
    }

    public void setMaxLengthErrorMessage(String maxLengthErrorMessage) {
        this.maxLengthErrorMessage = maxLengthErrorMessage;
    }

    public void setEmptyErrorMessage(String emptyErrorMessage) {
        this.emptyErrorMessage = emptyErrorMessage;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }
}
