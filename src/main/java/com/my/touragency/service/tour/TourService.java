package com.my.touragency.service.tour;

import com.my.touragency.common.Model;


/**
 * Created by Stein on 20.04.17.
 */
public interface TourService {
    String createTour(Model model);

    String showCreateTourPage(Model model);

    String showUpdateTourPage(Model model);

    String updateTour(Model model);

    String deleteTour(Model model);

    String showTourListPage(Model model);

    String showTourDetailPage(Model model);
}
