package com.my.touragency.service.tour;

import com.my.touragency.common.Model;
import com.my.touragency.common.constants.Message;
import com.my.touragency.common.constants.Key;
import com.my.touragency.common.constants.Page;
import com.my.touragency.common.constants.Redirection;
import com.my.touragency.context.ApplicationContext;
import com.my.touragency.domain.Tour;
import com.my.touragency.domain.TourType;
import com.my.touragency.persistance.dao.DaoContext;
import com.my.touragency.service.parsers.*;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Stein on 20.04.17.
 */
public class TourServiceImpl implements TourService {
    private static final Logger LOGGER = Logger.getLogger(TourServiceImpl.class);
    private DaoContext dc = ApplicationContext.getDaoContext();
    private static final int MAX_TITLE = 64;
    private static final int MAX_ROUTE = 128;
    private static final int MAX_DESCRIPTION = 256;
    private static final int MAX_DISCOUNT = 100;
    private static final String CREATE_TOUR_LOG_MSG = "Tour id %d created by user id %d";
    private static final String UPDATE_TOUR_LOG_MSG = "Tour id %d updated by user id %d";
    private static final String DELETE_TOUR_LOG_MSG = "Tour id %d deleted by user id %d";
    private static final String SHOW_UPDATE_PAGE_LOG_MSG = "Show update page for tour id %d";
    private static final String SHOW_DETAIL_PAGE_LOG_MSG = "Show detail page for tour id %d";

    @Override
    public String createTour(Model model) {
        Map<String, Object> errors = new HashMap<>();
        Tour tour = new Tour();
        tour.setTitle(parseTitle(errors, model));
        tour.setRoute(parseRoute(errors, model));
        tour.setDescription(parseDescription(errors, model));
        tour.setPrice(parsePrice(errors, model));
        tour.setDiscount(parseDiscount(errors, model));
        tour.setType(parseType(model));
        tour.setActive(parseActive(model));
        tour.setHot(parseHot(model));
        if (!errors.isEmpty()) {
            model.setAttribute(Key.TOUR_TYPES, TourType.values());
            model.setAttributes(errors);
            return Page.TOUR_CREATE;
        }
        dc.getTourDao().create(tour);
        LOGGER.info(String.format(CREATE_TOUR_LOG_MSG, tour.getId(), model.getUser().getId()));
        return Redirection.TOUR_LIST;
    }

    @Override
    public String updateTour(Model model) {
        Map<String, Object> errors = new HashMap<>();
        Tour tour = dc.getTourDao().read(getTourId(model));
        tour.setTitle(parseTitle(errors, model));
        tour.setRoute(parseRoute(errors, model));
        tour.setDescription(parseDescription(errors, model));
        tour.setPrice(parsePrice(errors, model));
        tour.setDiscount(parseDiscount(errors, model));
        tour.setType(parseType(model));
        tour.setActive(parseActive(model));
        tour.setHot(parseHot(model));
        if (!errors.isEmpty()) {
            model.setAttribute(Key.TOUR, tour);
            model.setAttribute(Key.TOUR_TYPES, TourType.values());
            model.setAttributes(errors);
            return Page.TOUR_UPDATE;
        }
        dc.getTourDao().update(tour);
        LOGGER.info(String.format(UPDATE_TOUR_LOG_MSG, tour.getId(), model.getUser().getId()));
        return Redirection.TOUR_LIST;
    }

    @Override
    public String deleteTour(Model model) {
        Tour tour = dc.getTourDao().read(getTourId(model));
        tour.setActive(false);
        dc.getTourDao().update(tour);
        LOGGER.info(String.format(DELETE_TOUR_LOG_MSG, tour.getId(), model.getUser().getId()));
        return Redirection.TOUR_LIST;
    }

    @Override
    public String showCreateTourPage(Model model) {
        model.setAttribute(Key.TOUR_TYPES, TourType.values());
        return Page.TOUR_CREATE;
    }

    @Override
    public String showUpdateTourPage(Model model) {
        Tour tour = dc.getTourDao().read(getTourId(model));
        model.setAttribute(Key.TOUR, tour);
        model.setAttribute(Key.TOUR_TYPES, TourType.values());
        LOGGER.info(String.format(SHOW_UPDATE_PAGE_LOG_MSG, tour.getId()));
        return Page.TOUR_UPDATE;
    }

    @Override
    public String showTourListPage(Model model) {
        Collection<Tour> tours = dc.getTourDao().readAll();
        for (Tour tour : tours) {
            tour.setFinalPrice(calculateFinalPrice(tour.getPrice(), tour.getDiscount()));
        }
        model.setAttribute(Key.TOURS, tours);
        return Page.TOUR_LIST;
    }

    @Override
    public String showTourDetailPage(Model model) {
        Tour tour = dc.getTourDao().read(getTourId(model));
        tour.setFinalPrice(calculateFinalPrice(tour.getPrice(), tour.getDiscount()));
        model.setAttribute(Key.TOUR, tour);
        LOGGER.info(String.format(SHOW_DETAIL_PAGE_LOG_MSG, tour.getId()));
        return Page.TOUR_DETAIL;
    }

    private Long calculateFinalPrice(Long price, Integer discount) {
        return price - (price * discount / Key.HUNDRED);
    }

    private Long getTourId(Model model) {
        return Long.parseLong(model.findParameter(Key.ID));
    }

    private String parseTitle(Map<String, Object> errors, Model model) {
        StringRequiredParser titleParser = new StringRequiredParser(errors);
        titleParser.setMaxLength(MAX_TITLE);
        titleParser.setMaxLengthErrorMessage(Message.TITLE);
        return model.findParameter(Key.TOUR_TITLE, titleParser);
    }

    private String parseRoute(Map<String, Object> errors, Model model) {
        StringRequiredParser routeParser = new StringRequiredParser(errors);
        routeParser.setMaxLength(MAX_ROUTE);
        routeParser.setMaxLengthErrorMessage(Message.ROUTE);
        return model.findParameter(Key.TOUR_ROUTE, routeParser);
    }

    private Long parsePrice(Map<String, Object> errors, Model model) {
        return model.findParameter(Key.TOUR_PRICE, new LongParser(errors));
    }

    private Integer parseDiscount(Map<String, Object> errors, Model model) {
        IntegerParser discountParser = new IntegerParser(errors);
        discountParser.setMaxLength(MAX_DISCOUNT);
        discountParser.setMaxLengthErrorMessage(Message.DISCOUNT);
        return model.findParameter(Key.TOUR_DISCOUNT, discountParser);
    }

    private String parseDescription(Map<String, Object> errors, Model model) {
        StringRequiredParser descriptionParser = new StringRequiredParser(errors);
        descriptionParser.setMaxLength(MAX_DESCRIPTION);
        descriptionParser.setMaxLengthErrorMessage(Message.DESCRIPTION);
        return model.findParameter(Key.TOUR_DESCRIPTION, descriptionParser);
    }

    private TourType parseType(Model model) {
        return model.findParameter(Key.TOUR_TYPE, new TourTypeParser());
    }

    private boolean parseHot(Model model) {
        return model.findParameter(Key.TOUR_HOT, new BooleanParser());
    }

    private boolean parseActive(Model model) {
        return model.findParameter(Key.TOUR_ACTIVE, new BooleanParser());
    }

}
