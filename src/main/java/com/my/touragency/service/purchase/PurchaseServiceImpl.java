package com.my.touragency.service.purchase;

import com.my.touragency.common.Model;
import com.my.touragency.common.constants.Key;
import com.my.touragency.common.constants.Page;
import com.my.touragency.common.constants.Redirection;
import com.my.touragency.context.ApplicationContext;
import com.my.touragency.domain.Purchase;
import com.my.touragency.domain.Tour;
import com.my.touragency.domain.User;
import com.my.touragency.domain.dto.TourPurchasesDTO;
import com.my.touragency.persistance.dao.DaoContext;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stein on 08.05.17.
 */
public class PurchaseServiceImpl implements PurchaseService {
    private DaoContext dc = ApplicationContext.getDaoContext();


    @Override
    public String tourPurchase(Model model) {
        Tour tour = dc.getTourDao().read(Long.parseLong(model.findParameter(Key.ID)));
        Purchase purchase = new Purchase();
        purchase.setPurchaseDate(LocalDateTime.now());
        purchase.setTour(tour);
        purchase.setUser((User) model.getSessionAttribute(Key.USER));
        purchase.setFinalPrice(calculateFinalPrice(tour.getPrice(), tour.getDiscount()));
        purchase.setStatus(true);
        dc.getPurchaseDao().create(purchase);
        return Redirection.PURCHASE_LIST;
    }

    @Override
    public String showPurchaseListPage(Model model) {
        Collection<Purchase> purchasesList = dc.getPurchaseDao().findPurchasesByUser(model.getUser().getId());
        Map<Long, TourPurchasesDTO> purchasesMap = new HashMap<>();
        for (Purchase purchase : purchasesList) {
            TourPurchasesDTO tourPurchasesDTO = findOrCreatePurchaseDto(purchasesMap, purchase.getTour());
            tourPurchasesDTO.getPurchases().add(purchase);
            tourPurchasesDTO.setQuantity(tourPurchasesDTO.getQuantity() + 1);
            tourPurchasesDTO.setTotalPrice(tourPurchasesDTO.getTotalPrice() + purchase.getFinalPrice());
            if (tourPurchasesDTO.getLastPurchaseDate() == null
                    || tourPurchasesDTO.getLastPurchaseDate().isBefore(purchase.getPurchaseDate())) {
                tourPurchasesDTO.setLastPurchaseDate(purchase.getPurchaseDate());
            }
        }
        model.setAttribute(Key.PURCHASES, purchasesMap.values());
        return Page.PURCHASE_LIST;
    }

    private TourPurchasesDTO findOrCreatePurchaseDto(Map<Long, TourPurchasesDTO> map, Tour tour) {
        TourPurchasesDTO purchasesDTO = map.get(tour.getId());
        if (purchasesDTO == null) {
            purchasesDTO = new TourPurchasesDTO();
            purchasesDTO.setTour(tour);
            map.put(tour.getId(), purchasesDTO);
        }
        return purchasesDTO;
    }


    private Long calculateFinalPrice(Long price, Integer discount) {
        return price - (price * discount / Key.HUNDRED);
    }
}
