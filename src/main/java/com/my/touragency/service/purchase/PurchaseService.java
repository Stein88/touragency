package com.my.touragency.service.purchase;

import com.my.touragency.common.Model;

/**
 * Created by Stein on 08.05.17.
 */
public interface PurchaseService {
    String tourPurchase(Model model);

    String showPurchaseListPage(Model model);

}
