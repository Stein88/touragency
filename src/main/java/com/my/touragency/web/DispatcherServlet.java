package com.my.touragency.web;


import com.my.touragency.context.ApplicationContext;
import com.my.touragency.controller.ControllerContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.my.touragency.web.HttpMethod.GET;
import static com.my.touragency.web.HttpMethod.POST;

/**
 * Created by Stein on 19.04.17.
 */
public class DispatcherServlet extends HttpServlet {
    private static ControllerContext cc = ApplicationContext.getControllerContext();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        cc.process(GET, req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        cc.process(POST, req, resp);
    }
}