package com.my.touragency.web;

/**
 * Created by Stein on 19.04.17.
 */
public enum HttpMethod {
    GET("GET"), POST("POST");

    private String name;

    HttpMethod(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
