package com.my.touragency.common.constants;

/**
 * Created by Stein on 13.05.17.
 */
public interface Message {
    String MAX_USER_NAME = "Should contains no more than 10 characters";
    String MAX_USER_PASSWORD = "Should contains no more than 6 characters";
    String DISCOUNT = "Discount should be no more then 100";
    String TITLE = "Title should contains no more than 20 characters";
    String ROUTE = "Route should contains no more than 25 characters";
    String DESCRIPTION = "Description should contains no more than 256 characters";
    String USER_ALREADY_CREATED = "This user already created";
    String INCORRECT_EMAIL_PASSWORD = "Incorrect email or password";
}
