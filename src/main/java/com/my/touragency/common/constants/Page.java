package com.my.touragency.common.constants;

/**
 * Created by Stein on 12.05.17.
 */
public interface Page {
    String LOGIN = "/views/login.jsp";
    String USER_LIST = "/views/user_list.jsp";
    String BAD_URL = "/views/404.jsp";
    String ERROR = "/views/error.jsp";
    String TOUR_CREATE = "/views/tour_create.jsp";
    String TOUR_UPDATE = "/views/tour_update.jsp";
    String TOUR_LIST = "/views/tour_list.jsp";
    String TOUR_DETAIL = "/views/tour_detail.jsp";
    String PURCHASE_LIST = "/views/purchase_list.jsp";

}
