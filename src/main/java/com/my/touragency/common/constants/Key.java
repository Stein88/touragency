package com.my.touragency.common.constants;

/**
 * Created by Stein on 13.05.17.
 */
public interface Key {
    /*
    Tour key constants
     */
    String TOUR_TYPES = "types";
    String TOUR_TYPE = "type";
    String TOUR = "tour";
    String TOUR_TITLE = "title";
    String TOUR_ROUTE = "route";
    String TOUR_PRICE = "price";
    String TOUR_DISCOUNT = "discount";
    String TOUR_DESCRIPTION = "description";
    String TOUR_HOT = "hot";
    String TOUR_ACTIVE = "active";
    String TOURS = "tours";

    /*
    User key constants
     */
    String EMAIL = "email";
    String PASSWORD = "password";
    String USERNAME = "username";
    String USER = "user";
    String USERS = "users";

    /*
    Purchase key
     */
    String PURCHASES = "purchases";


    String ID = "id";
    String REGISTRATION_ERROR = "registration_error";
    String LOGIN_ERROR = "login_error";
    String SHOW_REGISTER_FORM = "showRegisterForm";
    int HUNDRED = 100;
}
