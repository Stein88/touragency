package com.my.touragency.common.constants;

/**
 * Created by Stein on 12.05.17.
 */
public interface Redirection {
    String LOGIN = "redirect:/app/login";
    String TOUR_LIST = "redirect:/app/tour/list";
    String PURCHASE_LIST = "redirect:/app/purchase/list";

}
