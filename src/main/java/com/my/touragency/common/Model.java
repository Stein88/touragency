package com.my.touragency.common;

import com.my.touragency.domain.User;
import com.my.touragency.service.parsers.Parser;

import java.util.Map;

/**
 * Created by Stein on 19.04.17.
 */
public interface Model {
    void setAttribute(String key, Object o);

    void setAttributes(Map<String, Object> attributes);

    String findParameter(String key);

    <T> T findParameter(String key, Parser<T> parser);

    void putSessionAttribute(String s, Object o);

    Object getSessionAttribute(String s);

    User getUser();

}
