package com.my.touragency.persistance.dao;

import com.my.touragency.persistance.dao.purchase.PurchaseDao;
import com.my.touragency.persistance.dao.purchase.PurchaseDaoImpl;
import com.my.touragency.persistance.dao.tour.TourDao;
import com.my.touragency.persistance.dao.tour.TourDaoImpl;
import com.my.touragency.persistance.dao.user.UserDao;
import com.my.touragency.persistance.dao.user.UserDaoImpl;

/**
 * Created by Stein on 20.04.17.
 */
public class DaoFactoryImpl implements DaoFactory {

    @Override
    public TourDao createTourDao() {
        return new TourDaoImpl();
    }

    @Override
    public UserDao createUserDao() {
        return new UserDaoImpl();
    }

    @Override
    public PurchaseDao createPurchaseDao() {
        return new PurchaseDaoImpl();
    }
}
