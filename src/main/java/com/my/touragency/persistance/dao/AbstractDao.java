package com.my.touragency.persistance.dao;

import com.my.touragency.persistance.db.DataCommand;

/**
 * Created by Stein on 15.05.17.
 */
public abstract class AbstractDao {

    protected  <T> T executeDataCommand(DataCommand<T> command) {
        return command.execute();
    }
}
