package com.my.touragency.persistance.dao;

import com.my.touragency.persistance.dao.purchase.PurchaseDao;
import com.my.touragency.persistance.dao.tour.TourDao;
import com.my.touragency.persistance.dao.user.UserDao;

/**
 * Created by Stein on 20.04.17.
 */
public class DaoContext {
    private TourDao tourDao;
    private UserDao userDao;
    private PurchaseDao purchaseDao;

    public static DaoContext init(DaoFactory factory) {
        DaoContext daoContext = new DaoContext();
        daoContext.tourDao = factory.createTourDao();
        daoContext.userDao = factory.createUserDao();
        daoContext.purchaseDao = factory.createPurchaseDao();
        return daoContext;
    }

    public TourDao getTourDao() {
        return tourDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public PurchaseDao getPurchaseDao() {
        return purchaseDao;
    }
}
