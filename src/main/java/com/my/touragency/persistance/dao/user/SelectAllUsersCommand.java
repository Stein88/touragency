package com.my.touragency.persistance.dao.user;

import com.my.touragency.persistance.db.SelectQuery;
import com.my.touragency.domain.User;
import com.my.touragency.domain.UserRole;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Stein on 04.05.17.
 */
public class SelectAllUsersCommand extends SelectQuery<Collection<User>> {
    private final static String QUERY = "SELECT * FROM user";

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        //do nothing
    }

    @Override
    public Collection<User> parseResultSet(ResultSet resultSet) throws SQLException {
        Collection<User> usersList = new ArrayList<>();
        while (resultSet.next()) {
            User user = new User();
            user.setId(resultSet.getLong("user_id"));
            user.setUsername(resultSet.getString("username"));
            user.setEmail(resultSet.getString("email"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(UserRole.fromString(resultSet.getString("role")));
            usersList.add(user);
        }
        return usersList;
    }
}
