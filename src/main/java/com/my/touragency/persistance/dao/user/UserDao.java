package com.my.touragency.persistance.dao.user;

import com.my.touragency.domain.User;

import java.util.Collection;

/**
 * Created by Stein on 25.04.17.
 */
public interface UserDao {
    User create(User user);

    User findByEmail(String email);

    Collection<User> readAll();

}
