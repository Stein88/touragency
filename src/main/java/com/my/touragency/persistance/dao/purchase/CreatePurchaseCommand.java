package com.my.touragency.persistance.dao.purchase;

import com.my.touragency.persistance.db.InsertQuery;
import com.my.touragency.domain.Purchase;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Created by Stein on 08.05.17.
 */
public class CreatePurchaseCommand extends InsertQuery<Purchase> {
    private static final String QUERY = "INSERT INTO purchase(date,tour_id,user_id,final_price,status) " +
            "VALUES (?,?,?,?,?)";
    private Purchase purchase;

    CreatePurchaseCommand(Purchase purchase) {
        this.purchase = purchase;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setTimestamp(1, Timestamp.valueOf(purchase.getPurchaseDate()));
        preparedStatement.setLong(2, purchase.getTour().getId());
        preparedStatement.setLong(3, purchase.getUser().getId());
        preparedStatement.setLong(4, purchase.getFinalPrice());
        preparedStatement.setBoolean(5, purchase.getStatus());
    }

    @Override
    public Purchase extractInserted(Long id) throws SQLException {
        purchase.setId(id);
        return purchase;
    }
}
