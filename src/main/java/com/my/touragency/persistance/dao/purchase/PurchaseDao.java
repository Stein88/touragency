package com.my.touragency.persistance.dao.purchase;

import com.my.touragency.domain.Purchase;

import java.util.Collection;

/**
 * Created by Stein on 08.05.17.
 */
public interface PurchaseDao {
    Purchase create(Purchase purchase);
    Collection<Purchase> findPurchasesByUser(Long userId);
}
