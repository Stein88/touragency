package com.my.touragency.persistance.dao.tour;

import com.my.touragency.persistance.dao.AbstractDao;
import com.my.touragency.domain.Tour;

import java.util.Collection;

/**
 * Created by Stein on 20.04.17.
 */
public class TourDaoImpl extends AbstractDao implements TourDao {

    @Override
    public Collection<Tour> readAll() {
        return executeDataCommand(new SelectAllToursCommand());
    }

    @Override
    public Tour create(Tour tour) {
        return executeDataCommand(new CreateTourCommand(tour));
    }

    @Override
    public Tour read(Long tourId) {
        return executeDataCommand(new SelectTourByIdCommand(tourId));
    }

    @Override
    public Tour update(Tour tour) {
        return executeDataCommand(new UpdateTourCommand(tour));
    }
}
