package com.my.touragency.persistance.dao;

import com.my.touragency.persistance.dao.purchase.PurchaseDao;
import com.my.touragency.persistance.dao.tour.TourDao;
import com.my.touragency.persistance.dao.user.UserDao;

/**
 * Created by Stein on 20.04.17.
 */
public interface DaoFactory {
    TourDao createTourDao();

    UserDao createUserDao();

    PurchaseDao createPurchaseDao();
}

