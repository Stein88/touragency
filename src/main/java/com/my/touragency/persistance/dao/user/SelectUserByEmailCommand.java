package com.my.touragency.persistance.dao.user;


import com.my.touragency.persistance.db.SelectQuery;
import com.my.touragency.domain.User;
import com.my.touragency.domain.UserRole;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Stein on 25.04.17.
 */
public class SelectUserByEmailCommand extends SelectQuery<User> {
    private static final String QUERY = "SELECT * FROM user WHERE email = ?";
    private String email;

    SelectUserByEmailCommand(String email) {
        this.email = email;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, email);

    }

    @Override
    public User parseResultSet(ResultSet resultSet) throws SQLException {
        User user = null;
        while (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getLong("user_id"));
            user.setUsername(resultSet.getString("username"));
            user.setEmail(resultSet.getString("email"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(UserRole.fromString(resultSet.getString("role")));
        }
        return user;
    }
}
