package com.my.touragency.persistance.dao.tour;

import com.my.touragency.domain.Tour;

import java.util.Collection;

/**
 * Created by Stein on 20.04.17.
 */
public interface TourDao {
    Collection<Tour> readAll();
    Tour create(Tour tour);
    Tour read(Long tourId);
    Tour update(Tour tour);
}
