package com.my.touragency.persistance.dao.tour;

import com.my.touragency.persistance.db.SelectQuery;
import com.my.touragency.domain.Tour;
import com.my.touragency.domain.TourType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Stein on 20.04.17.
 */
public class SelectAllToursCommand extends SelectQuery<Collection<Tour>> {
    private static final String QUERY = "SELECT * FROM tour WHERE active = true";

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        //do nothing
    }

    @Override
    public Collection<Tour> parseResultSet(ResultSet resultSet) throws SQLException {
        Collection<Tour> tourList = new ArrayList<>();
        while (resultSet.next()) {
            Tour tour = new Tour();
            tour.setId(resultSet.getLong("tour_id"));
            tour.setTitle(resultSet.getString("title"));
            tour.setRoute(resultSet.getString("route"));
            tour.setPrice(resultSet.getLong("price"));
            tour.setDescription(resultSet.getString("description"));
            tour.setDiscount(resultSet.getInt("discount"));
            tour.setType(TourType.fromString(resultSet.getString("type")));
            tour.setHot(resultSet.getBoolean("hot"));
            tour.setActive(resultSet.getBoolean("active"));
            tourList.add(tour);
        }
        return tourList;
    }
}
