package com.my.touragency.persistance.dao.user;

import com.my.touragency.persistance.db.InsertQuery;
import com.my.touragency.domain.User;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Stein on 25.04.17.
 */
public class InsertUserCommand extends InsertQuery<User> {
    private static final String QUERY = "INSERT INTO user(username, email, password, role) VALUES (?, ?, ?, ?)";
    private User user;

    InsertUserCommand(User user) {
        this.user = user;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, user.getUsername());
        preparedStatement.setString(2, user.getEmail());
        preparedStatement.setString(3, user.getPassword());
        preparedStatement.setString(4, user.getRole().toString());
    }

    @Override
    public User extractInserted(Long generatedId) throws SQLException {
        user.setId(generatedId);
        return user;
    }

}
