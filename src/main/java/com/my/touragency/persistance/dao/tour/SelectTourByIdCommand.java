package com.my.touragency.persistance.dao.tour;

import com.my.touragency.persistance.db.SelectQuery;
import com.my.touragency.domain.Tour;
import com.my.touragency.domain.TourType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Stein on 05.05.17.
 */
public class SelectTourByIdCommand extends SelectQuery<Tour> {
    private static final String QUERY = "SELECT * FROM tour WHERE tour_id = ?";
    private Long id;

    public SelectTourByIdCommand(Long id) {
        this.id = id;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, id);
    }

    @Override
    public Tour parseResultSet(ResultSet resultSet) throws SQLException {
        Tour tour = new Tour();
        while (resultSet.next()) {
            tour.setId(resultSet.getLong("tour_id"));
            tour.setTitle(resultSet.getString("title"));
            tour.setRoute(resultSet.getString("route"));
            tour.setPrice(resultSet.getLong("price"));
            tour.setDescription(resultSet.getString("description"));
            tour.setDiscount(resultSet.getInt("discount"));
            tour.setType(TourType.fromString(resultSet.getString("type")));
            tour.setHot(resultSet.getBoolean("hot"));
            tour.setActive(resultSet.getBoolean("active"));
        }
        return tour;
    }
}
