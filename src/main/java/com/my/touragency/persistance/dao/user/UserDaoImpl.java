package com.my.touragency.persistance.dao.user;



import com.my.touragency.persistance.dao.AbstractDao;
import com.my.touragency.domain.User;

import java.util.Collection;

/**
 * Created by Stein on 25.04.17.
 */
public class UserDaoImpl extends AbstractDao implements UserDao  {

    @Override
    public User create(User user) {
        return executeDataCommand(new InsertUserCommand(user));
    }

    @Override
    public User findByEmail(String email) {
        return executeDataCommand(new SelectUserByEmailCommand(email));
    }

    @Override
    public Collection<User> readAll() {
        return executeDataCommand(new SelectAllUsersCommand());
    }
}
