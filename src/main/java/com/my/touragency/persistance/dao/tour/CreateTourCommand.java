package com.my.touragency.persistance.dao.tour;

import com.my.touragency.domain.Tour;
import com.my.touragency.persistance.db.InsertQuery;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Stein on 21.04.17.
 */
public class CreateTourCommand extends InsertQuery<Tour> {
    private Tour tour;
    private static final String QUERY = "INSERT INTO tour(title, route, price, description, discount, type, active, hot) " +
            "VALUES (?,?,?,?,?,?,?,?)";

    public CreateTourCommand(Tour tour) {
        this.tour = tour;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, tour.getTitle());
        preparedStatement.setString(2, tour.getRoute());
        preparedStatement.setLong(3, tour.getPrice());
        preparedStatement.setString(4, tour.getDescription());
        preparedStatement.setInt(5, tour.getDiscount());
        preparedStatement.setString(6, tour.getType().toString());
        preparedStatement.setBoolean(7, tour.getActive());
        preparedStatement.setBoolean(8, tour.getHot());

    }

    @Override
    public Tour extractInserted(Long generatedId) throws SQLException {
        tour.setId(generatedId);
        return tour;
    }


}
