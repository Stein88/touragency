package com.my.touragency.persistance.dao.purchase;

import com.my.touragency.persistance.db.SelectQuery;
import com.my.touragency.domain.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Stein on 09.05.17.
 */
public class FindPurchasesByUserCommand extends SelectQuery<Collection<Purchase>> {
    private static final String QUERY = "SELECT * FROM " +
            "purchase p JOIN tour t ON p.tour_id = t.tour_id " +
            "JOIN user u ON p.user_id = u.user_id " +
            "WHERE p.user_id = ?";
    private Long userId;

    FindPurchasesByUserCommand(Long userId) {
        this.userId = userId;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, userId);
    }

    @Override
    public Collection<Purchase> parseResultSet(ResultSet resultSet) throws SQLException {
        Collection<Purchase> purchasesList = new ArrayList<>();
        while (resultSet.next()) {
            Purchase purchase = new Purchase();
            purchase.setId(resultSet.getLong("purchase_id"));
            Timestamp timestamp = resultSet.getTimestamp("date");
            purchase.setPurchaseDate(timestamp.toLocalDateTime());
            Tour tour = new Tour();
            tour.setId(resultSet.getLong("tour_id"));
            tour.setTitle(resultSet.getString("title"));
            tour.setPrice(resultSet.getLong("price"));
            tour.setDescription(resultSet.getString("description"));
            tour.setDiscount(resultSet.getInt("discount"));
            tour.setType(TourType.fromString(resultSet.getString("type")));
            tour.setActive(resultSet.getBoolean("active"));
            tour.setHot(resultSet.getBoolean("hot"));
            purchase.setTour(tour);
            User user = new User();
            user.setId(resultSet.getLong("user_id"));
            user.setUsername(resultSet.getString("username"));
            user.setEmail(resultSet.getString("email"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(UserRole.fromString(resultSet.getString("role")));
            purchase.setUser(user);
            purchase.setFinalPrice(resultSet.getLong("final_price"));
            purchase.setStatus(resultSet.getBoolean("status"));
            purchasesList.add(purchase);
        }
        return purchasesList;
    }
}
