package com.my.touragency.persistance.dao.tour;

import com.my.touragency.persistance.db.UpdateQuery;
import com.my.touragency.domain.Tour;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Stein on 07.05.17.
 */
public class UpdateTourCommand extends UpdateQuery<Tour> {
    private static final String QUERY = "UPDATE tour SET title = ?, route = ?, price = ?, description = ?, discount = ?, type = ?," +
            " active = ?, hot = ?  WHERE tour_id = ?";
    private Tour tour;

    UpdateTourCommand(Tour tour) {
        this.tour = tour;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, tour.getTitle());
        preparedStatement.setString(2, tour.getRoute());
        preparedStatement.setLong(3, tour.getPrice());
        preparedStatement.setString(4, tour.getDescription());
        preparedStatement.setInt(5, tour.getDiscount());
        preparedStatement.setString(6, tour.getType().toString());
        preparedStatement.setBoolean(7, tour.getActive());
        preparedStatement.setBoolean(8, tour.getHot());
        preparedStatement.setLong(9, tour.getId());
    }

    @Override
    public Tour returnUpdatedObject() throws SQLException {
        return tour;
    }

}
