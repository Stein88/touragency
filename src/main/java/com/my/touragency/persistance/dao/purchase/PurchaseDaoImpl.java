package com.my.touragency.persistance.dao.purchase;

import com.my.touragency.persistance.dao.AbstractDao;
import com.my.touragency.domain.Purchase;

import java.util.Collection;

/**
 * Created by Stein on 08.05.17.
 */
public class PurchaseDaoImpl extends AbstractDao implements PurchaseDao {

    @Override
    public Purchase create(Purchase purchase) {
        return executeDataCommand(new CreatePurchaseCommand(purchase));
    }

    @Override
    public Collection<Purchase> findPurchasesByUser(Long userId) {
        return executeDataCommand(new FindPurchasesByUserCommand(userId));
    }
}
