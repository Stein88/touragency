package com.my.touragency.persistance.db;

/**
 * Created by Stein on 15.05.17.
 */
public interface DataCommand<T> {
    T execute();
}
