package com.my.touragency.persistance.db;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Stein on 15.05.17.
 */
public abstract class SelectQuery<T> implements DataCommand<T> {
    private static final Logger LOGGER = Logger.getLogger(SelectQuery.class);
    private Connection connection = ConnectionManager.getInstance().getOrCreateConnection();

    @Override
    public T execute() {
        String sql = getQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            setupStatement(preparedStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            return parseResultSet(resultSet);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    protected abstract String getQuery();

    protected abstract void setupStatement(PreparedStatement preparedStatement) throws SQLException;

    protected abstract T parseResultSet(ResultSet resultSet) throws SQLException;
}
