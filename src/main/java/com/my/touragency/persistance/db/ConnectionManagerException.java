package com.my.touragency.persistance.db;

/**
 * Created by Stein on 17.05.17.
 */
public class ConnectionManagerException extends RuntimeException {
    public ConnectionManagerException(Throwable cause) {
        super(cause);
    }
}
