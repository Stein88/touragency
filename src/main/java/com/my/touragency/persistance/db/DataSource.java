package com.my.touragency.persistance.db;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Stein on 17.05.17.
 */
public class DataSource {
    private static DataSource instance = new DataSource();
    private BasicDataSource basicDataSource;
    private final Logger LOGGER = Logger.getLogger(DataSource.class);


    private DataSource() {
        initDataSource();
    }

    private void initDataSource() {
        basicDataSource = new BasicDataSource();
        Properties properties = new Properties();

        try (InputStream inStream = getClass().getResourceAsStream("/db.properties")) {
            properties.load(inStream);
            basicDataSource.setDriverClassName(properties.getProperty("driver.class"));
        } catch (IOException e) {
            LOGGER.error("Some problem was occurred while reading property file \n" + e);
        }

        basicDataSource.setUrl(properties.getProperty("url"));
        basicDataSource.setUsername(properties.getProperty("user"));
        basicDataSource.setPassword(properties.getProperty("password"));
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = basicDataSource.getConnection();
        } catch (SQLException e) {
            LOGGER.error("Some problem was occurred while getting connection to BD \n" + e);
        }
        return connection;
    }

    public static DataSource getInstance() {
        return instance;
    }

}
