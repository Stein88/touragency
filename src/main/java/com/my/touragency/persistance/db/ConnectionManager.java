package com.my.touragency.persistance.db;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Stein on 14.05.17.
 */
public class ConnectionManager {
    private static final Logger LOGGER = Logger.getLogger(ConnectionManager.class);
    private DataSource dataSource = DataSource.getInstance();
    private static ConnectionManager instance = new ConnectionManager();
    private ThreadLocal<Connection> threadConnection = new ThreadLocal<>();

    private ConnectionManager() {
    }

    public static ConnectionManager getInstance() {
        return instance;
    }

    Connection getOrCreateConnection() {
        try {
            Connection connection = threadConnection.get();
            if (!isConnectionActive(connection)) {
                connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                threadConnection.set(connection);
            }
            return connection;
        } catch (SQLException e) {
            LOGGER.warn("could get connection", e);
            throw new ConnectionManagerException(e);
        }
    }


    public void commit() {
        try {
            Connection connection = threadConnection.get();
            if (isConnectionActive(connection)) {
                connection.commit();
            }
        } catch (SQLException e) {
            LOGGER.error("Commit exception!", e);
        }
    }

    public void rollback() {
        try {
            Connection connection = threadConnection.get();
            if (isConnectionActive(connection)) {
                connection.rollback();
            }
        } catch (SQLException e) {
            LOGGER.error("Rollback exception!", e);
        }
    }

    public void close() {
        try {
            Connection connection = threadConnection.get();
            if (isConnectionActive(connection)) {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Close exception!", e);
        }
    }

    private boolean isConnectionActive(Connection connection) throws SQLException {
        return connection != null && !connection.isClosed();
    }
}
