package com.my.touragency.persistance.db;


import org.apache.log4j.Logger;

import java.sql.*;

/**
 * Created by Stein on 15.05.17.
 */
public abstract class InsertQuery<T> implements DataCommand<T> {
    private static final Logger LOGGER = Logger.getLogger(InsertQuery.class);
    private Connection connection = ConnectionManager.getInstance().getOrCreateConnection();

    @Override
    public T execute() {
        String sql = getQuery();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            setupStatement(preparedStatement);
            int status = preparedStatement.executeUpdate();
            if (status != 1) {
                throw new SQLException("Creating failed, no rows affected.");
            }
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    Long generatedId = generatedKeys.getLong(1);
                    return extractInserted(generatedId);
                } else {
                    throw new SQLException("Creating failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    protected abstract String getQuery();

    protected abstract void setupStatement(PreparedStatement preparedStatement) throws SQLException;

    protected abstract T extractInserted(Long id) throws SQLException;
}
